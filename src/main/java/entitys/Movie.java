package entitys;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Movie {
	public Movie(){}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
   private int id;
	private String image;
   public Movie(String image, String name, String year, String detail,
			int money) {
		super();
		this.image = image;
		this.name = name;
		this.year = year;
		this.detail = detail;
		this.money = money;
	}
public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
public Movie(String name, String year, String detail, int money) {
		super();
		this.name = name;
		this.year = year;
		this.detail = detail;
		this.money = money;
	}
private String name;
   private String year;
   private String detail;
   private int money;
   @ManyToMany(cascade ={CascadeType.ALL})
  
   private Collection<Actor> actors=new ArrayList<Actor>();
  
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getYear() {
	return year;
}
public void setYear(String year) {
	this.year = year;
}
public String getDetail() {
	return detail;
}
public void setDetail(String detail) {
	this.detail = detail;
}
public int getMoney() {
	return money;
}
public void setMoney(int money) {
	this.money = money;
}
public Collection<Actor> getActors() {
	return actors;
}
public void setActors(Collection<Actor> actors) {
	this.actors = actors;
}


   
}
