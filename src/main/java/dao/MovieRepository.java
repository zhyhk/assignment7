package dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import entitys.Actor;
import entitys.Movie;

@Repository
public class MovieRepository {
	public MovieRepository(){}
	@Autowired
	private SessionFactory sessionFactory;
	  @Transactional
      public List<Movie> queryMovies(){
    	  Session s=sessionFactory.openSession();
    	  List<Movie> l=s.createQuery("from Movie").list();
    	  return l;
      }
	  @Transactional
      public Movie findMovie(int id){
		  Session s=sessionFactory.openSession();
    	  Query q=s.createQuery("from Movie where id=?");
    	  q.setParameter(0, id);
    	  Movie m=(Movie) q.uniqueResult();
    	  return m;
    	  
      }
	  @Transactional
      public List<Movie> searchMovie(String name){
		  Session s=sessionFactory.openSession();
    	  Query q=s.createQuery("from Movie where name like %?%");
    	  q.setParameter(0, name);
    	  List<Movie> l=q.list();
    	  Query q1=s.createQuery("from Actor where name like %?%");
    	  q.setParameter(0, name);
    	  List<Actor> l1=q.list();
    	  for(Actor a:l1){
    		  l.addAll(a.getMovies());     
    		  
    	  }
    	  return l;
    	  
      }
//	  @Transactional
//      public void saveUser(User user){
//		  Session s=sessionFactory.openSession();
//    	  s.save(user);
//      }
	  @Transactional
      public void saveMovies(){
		  Session s1=sessionFactory.openSession();
		  Movie m1=new Movie("1.jpg","Penguins of Madagascar","2014","Super spy teams aren’t born…they’re hatched. Skipper, Kowalski, Rico and Private now must join forces with the North Wind to stop the cunning villain Dr. Octavius Brine from taking over the world.",35);
			Movie m2=new Movie("2.jpg","Beyond the Lights","2014","Noni is the music world’s latest superstar. The pressures of fame have Noni on the edge - until she meets Kaz, a young cop who’s been assigned to her detail. Drawn to each other, Noni and Kaz fall fast and hard.",25);
			Movie m3=new Movie("3.jpg","Dumb and Dumber To","2014","Jim Carrey and Jeff Daniels reprise their signature roles as Lloyd and Harry in the sequel to the smash hit that took the physical comedy and kicked it in the nuts: Dumb and Dumber To. The original film’s directors, Peter and Bobby Farrelly, take...",35);
			Movie m4=new Movie("4.jpg","Exodus: Gods and Kings","2014","Ridley Scott brings new life to the story of the defiant leader Moses (Christian Bale) as he rises up against the Egyptian Pharaoh Ramses (Joel Edgerton).",33);
			Movie m5=new Movie("5.jpg","Foxcatcher","2014","When Olympic Gold Medal winning wrestler Mark Schultz is invited by wealthy heir John du Pont to move on to the du Pont estate and help form a team to train for the 1988 Seoul Olympics at his new state-of-the-art training facility, Schultz jumps at...",30);
			Movie m6=new Movie("6.jpg","Horrible Bosses 2","2014","After a shady investor steals their new company, Nick (Jason Bateman), Dale (Charlie Day) and Kurt (Jason Sudeikis) conspire to kidnap the man's adult son and ransom him to regain control.",29);
			Movie m7=new Movie("7.jpg","Interstellar","2014","With our time on Earth coming to an end, a team of explorers undertakes the most important mission in human history; traveling beyond this galaxy to discover whether mankind has a future among the stars.",31);
			Movie m8=new Movie("8.jpg","Into the Woods","2014","This humorous and heartfelt musical follows the classic tales of Cinderella (Anna Kendrick), Little Red Riding Hood (Lilla Crawford), Jack and the Beanstalk (Daniel Huttlestone), and Rapunzel (MacKenzie Mauzy)—all tied together by an original story...",38);
			Movie m9=new Movie("9.jpg","The Hunger Games: Mockingjay - Part 1","2014","The worldwide phenomenon of The Hunger Games continues to set the world on fire with The Hunger Games: Mockingjay - Part 1, which finds Katniss Everdeen (Jennifer Lawrence) in District 13 after she literally shatters the games forever.",15);
			Movie m10=new Movie("10.jpg","The Hobbit: The Battle of the Five Armies","2014","Men, Dwarves and Elves must unite or fall to the dual threats of Sauron's Orcs and Smaug the dragon.",35);
			Movie m11=new Movie("11.jpg","The Pyramid","2014","The ancient wonders of the world have long cursed explorers who've dared to uncover their secrets. But a team of U.S. archaeologists gets more than they bargained for when they discover a lost pyramid unlike any other in the Egyptian desert.",25);
			Movie m12=new Movie("12.jpg","Wild","2014","Adapted from Cheryl Strayed's best-selling memoir of the same name, director Jean-Marc Vallée's Wild stars Oscar-winner Reese Witherspoon as a self-destructive divorcée who seeks to conquer her demons by hiking 1100 miles across the Pacific Crest Trail.",20);
			
			
			
			
			Actor a1=new Actor("Kevin Rankin");
			Actor a5=new Actor("Bill Pohlad");
			Actor a2=new Actor("Bruna Papandrea");
			Actor a3=new Actor("Bergen Swanson");
			Actor a4=new Actor("Nathan Ross");
			Actor a6=new Actor("Nick Hornby");
			Actor a7=new Actor("Cheryl Strayed");
			Actor a8=new Actor("Jean-Marc Vallée");
			Actor a9=new Actor("Reese Witherspoon");
			Actor a10=new Actor("Thomas Sadoski");
			Actor a11=new Actor("Michiel Huisman");
			Actor a12=new Actor("Gaby Hoffmann");
			Actor a13=new Actor("Kevin Rankin");
			Actor a14=new Actor("Jenno Topping");
			Actor a15=new Actor("Mark Huffam");
			Actor a16=new Actor("Michael Schaefer");
			Actor a17=new Actor("Peter Chernin");
			Actor a18=new Actor("Adam Cooper");
			Actor a19=new Actor("Bill Collage");
			Actor a20=new Actor("Steven Zaillian");
			Actor a21=new Actor("Ridley Scott");
			Actor a22=new Actor("Christian Bale");
			Actor a23=new Actor("John Turturro");
			Actor a24=new Actor("Sigourney Weaver");
			Actor a25=new Actor("Aaron Paul");
			Actor a26=new Actor("Ben Kingsley");
			Actor a27=new Actor("Laurie Holden");
			Collection<Actor> l1=new ArrayList<Actor>();
			 m1.getActors().add(a1);
			 m1.getActors().add(a2);
			 m1.getActors().add(a3);
			 Collection<Actor> l2=new ArrayList<Actor>();
			 l2.add(a4);
			 l2.add(a5);
			 l2.add(a6);
			 Collection<Actor> l3=new ArrayList<Actor>();
			 l3.add(a7);
			 l3.add(a8);
			 l3.add(a9);
			 Collection<Actor> l4=new ArrayList<Actor>();
			 l4.add(a10);
			 l4.add(a11);
			 l4.add(a12);
			 Collection<Actor> l5=new ArrayList<Actor>();
			 l5.add(a13);
			 l5.add(a14);
			 l5.add(a15);
			 Collection<Actor> l6=new ArrayList<Actor>();
			 l6.add(a16);
			 l6.add(a17);
			 l6.add(a18);
			 Collection<Actor> l7=new ArrayList<Actor>();
			 l7.add(a19);
			 l7.add(a20);
			 l7.add(a21);
			 Collection<Actor> l8=new ArrayList<Actor>();
			 l8.add(a22);
			 l8.add(a23);
			 l8.add(a24);
			 Collection<Actor> l9=new ArrayList<Actor>();
			 l9.add(a25);
			 l9.add(a26);
			 l9.add(a27);
			 m1.setActors(l1);
			m2.setActors(l2);
			m3.setActors(l3);
			m4.setActors(l4);
			m5.setActors(l5);
			m6.setActors(l6);
			m7.setActors(l7);
			m8.setActors(l8);
			m9.setActors(l9);
			
			s1.save(m1);
			s1.save(m2);
			s1.save(m3);
			s1.save(m4);
			s1.save(m5);
			s1.save(m6);
			s1.save(m7);
			s1.save(m8);
			s1.save(m9);
			s1.save(m10);
			s1.save(m11);
			s1.save(m12);
			
			
      }
}
