package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.MovieRepository;
import entitys.Movie;

@Service

public class MovieService {
	@Autowired
      private MovieRepository  movieRepository;
	 public List<Movie> queryMovies(){
		return movieRepository.queryMovies();
     }
	 public void saveMovies(){
			 movieRepository.saveMovies();;
	     }
	
}
