package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import entitys.Movie;
import service.MovieService;



@Controller

public class MoviesController {
   @Autowired
	private MovieService movieService;
	@RequestMapping(value="/movies",method=RequestMethod.GET)
	
	public  @ResponseBody List<Movie>  getMovies() {
		return movieService.queryMovies();
   }
}
